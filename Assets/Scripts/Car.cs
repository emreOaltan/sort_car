using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;



public class Car : MonoBehaviour
{
    public const float CAR_SIZE = 25;
    public const float TIME_FACTOR_PER_DIST = 0.015f;
    public static float CAR_SPEED = 0.015f;


    public ColorType carColorType;

    private GraphPath myGraphPath;
    

    private void OnTriggerEnter(Collider other){
        DOTween.PauseAll();
        GameController.Instance.LevelFinished(false);
        other.enabled = false;
    }

    public void WaitingCarGoForward(){
        transform.DOMoveZ(transform.position.z + CAR_SIZE, 1f);
    }
    
    public bool Go(GraphPath path){
        if (path.parkPlace == null) return false;
        myGraphPath = path;
        
        Sequence pathSequence = DOTween.Sequence();
        Sequence rotateSequence = DOTween.Sequence();
        Vector3 latestTurningNode = Vector3.zero;
        for (int i = 0; i < path.pathNodes.Count - 1; i++){
            Vector3 toCurrentNode;
            if(latestTurningNode != Vector3.zero) toCurrentNode = path.pathNodes[i].transform.position - latestTurningNode;
            else toCurrentNode = path.pathNodes[i].transform.position - transform.position;
            
            Vector3 toNextNode = path.pathNodes[i+1].transform.position - path.pathNodes[i].transform.position;
            float angle = Vector3.Angle(toCurrentNode, toNextNode);
            if (angle > 15){
                float duration = toCurrentNode.magnitude * CAR_SPEED;
                pathSequence.Append(transform.DOMove(path.pathNodes[i].transform.position, duration).SetEase(Ease.InOutSine));
                rotateSequence.Append(transform.DORotateQuaternion(Quaternion.LookRotation(toNextNode), 0.2f)
                    .SetDelay(duration - (latestTurningNode == Vector3.zero ? 0.1f : 0.2f)).SetEase(Ease.Linear));
                latestTurningNode = path.pathNodes[i].transform.position;
            }
        }

        float dist = 100;
        if (latestTurningNode != Vector3.zero) dist = (path.parkPlace.transform.position - latestTurningNode).magnitude;
        
        pathSequence.Append(transform.DOMove(path.parkPlace.transform.position, dist * CAR_SPEED).SetEase(Ease.InOutSine));
        pathSequence.onComplete += CheckParkPlaceColor;
        
        return true;
    }

    private void CheckParkPlaceColor(){
        if (myGraphPath.parkPlace.colorType != carColorType){
            GameController.Instance.LevelFinished(false);
        }
        else{
            GameController.Instance.CheckLevelCompleted();
        }
    }
    
        
}
