using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class CarEntryController : MonoBehaviour{
    [SerializeField] private LevelController levelController;
    [SerializeField] private Graph roadGraph;
    [SerializeField] private Car[] purpleCarPrefabs;
    [SerializeField] private Car[] yellowCarPrefabs;
    [SerializeField] private Transform purpleDoor, yellowDoor;
    [SerializeField] private Transform purpleCarPosition, yellowCarPosition;
    [SerializeField] private GraphNode purpleStartNode, yellowStartNode;
    
    private List<Car> purpleWaitingCars, yellowWaitingCars;


    public int GetRemainingCarCount() => purpleWaitingCars.Count + yellowWaitingCars.Count;


    private void Start(){
        SpawnCars();
    }

    private void SpawnCars(){
        purpleWaitingCars = new List<Car>();
        yellowWaitingCars = new List<Car>();
        int purpCount = LevelController.Instance.PurpleParkPlaceCount;
        int yellCount = LevelController.Instance.YellowParkPlaceCount;

        
        Car tmpCar;
        for (int i = 0; i < purpCount; ++i){
            Vector3 _pos = purpleCarPosition.position - Vector3.forward * Car.CAR_SIZE * i;
            tmpCar = Instantiate(purpleCarPrefabs[Random.Range(0, purpleCarPrefabs.Length)], _pos, Quaternion.identity);
            purpleWaitingCars.Add(tmpCar);
        }
        for (int i = 0; i < yellCount; ++i){
            Vector3 _pos = yellowCarPosition.position - Vector3.forward * Car.CAR_SIZE * i;
            tmpCar = Instantiate(yellowCarPrefabs[Random.Range(0, yellowCarPrefabs.Length)], _pos, Quaternion.identity);
            yellowWaitingCars.Add(tmpCar);
        }
        
    }


    public void SendPurpleCar(MyButton myButton){
        Car.CAR_SPEED = Car.TIME_FACTOR_PER_DIST / levelController.carSpeed;
        
        DoorAnimation(purpleDoor, myButton);
        if(purpleWaitingCars.Count == 0) return;

        GraphPath graphPath = roadGraph.GetCarPath(purpleStartNode, purpleWaitingCars[0]);
        if (purpleWaitingCars[0].Go(graphPath)){
            purpleWaitingCars.RemoveAt(0);

            foreach (Car car in purpleWaitingCars){
                car.WaitingCarGoForward();
            }
        }
    }

    
    public void SendYellowCar(MyButton myButton){
        Car.CAR_SPEED = Car.TIME_FACTOR_PER_DIST / levelController.carSpeed;
        
        DoorAnimation(yellowDoor, myButton);
        if(yellowWaitingCars.Count == 0) return;
        
        GraphPath graphPath = roadGraph.GetCarPath(yellowStartNode, yellowWaitingCars[0]);
        if(yellowWaitingCars[0].Go(graphPath))
        {
            yellowWaitingCars.RemoveAt(0);
            foreach (Car car in yellowWaitingCars){
                car.WaitingCarGoForward();
            }
        }
    }
    
    private void DoorAnimation(Transform door, MyButton myButton){
        door.DORotate(new Vector3(0, 0, -90), levelController.doorOpeningTime).Play().OnComplete(() => {
            door.DORotate(new Vector3(0, 0, 0), levelController.doorOpeningTime).Play().SetDelay(levelController.doorOpenedWaitTime).OnComplete(() => {
                myButton.Release();
            });
        });
    }
}
