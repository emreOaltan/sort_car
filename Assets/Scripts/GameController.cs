using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState{Playing, Finished}

public class GameController : MonoBehaviour{
    public static GameController Instance;
    public static GameState gameState;

    private void Awake(){
        Instance = this;
        gameState = GameState.Playing;
    }

    [SerializeField] private UIController uIController;
    [SerializeField] private CarEntryController carEntryController;


    public void CheckLevelCompleted(){
        if (carEntryController.GetRemainingCarCount() == 0){
            LevelFinished(true);
        }
    }

    public void LevelFinished(bool isSuccess){
        if(gameState == GameState.Finished) return;
        
        uIController.LevelFinished(isSuccess);
        gameState = GameState.Finished;
    }

    public void RestartGame(){
        SceneManager.LoadScene(0);
    }

    public void NextLevel() => RestartGame();

}
