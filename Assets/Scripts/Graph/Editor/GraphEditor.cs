using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

[CustomEditor(typeof(Graph))]
[CanEditMultipleObjects]
public class GraphEditor : Editor
{
    SerializedProperty nodes;
    private Graph graph;
    
    void OnEnable()
    {
        nodes = serializedObject.FindProperty("nodes");
        graph = (Graph) target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        //EditorGUILayout.PropertyField(nodes);
        if (GUILayout.Button("Add Turn Point")){
           graph.PushNode();
        }

        if (GUILayout.Button("Update Neighbors")){
            graph.UpdateNeighbors();
        }
        
        EditorGUILayout.Space(40);
        if (GUILayout.Button("Reset Graph")){
            graph.ResetGraph();
        }
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI(){
        graph.DrawGraph();
    }

    
}
