using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GraphLine))]
[CanEditMultipleObjects]
public class GraphLineEditor : Editor
{
    private GraphLine graphLine;
    void OnEnable(){
        graphLine = (GraphLine) target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        if (GUILayout.Button("Insert Node")){
            graphLine.InsertNode();
        }
        
        EditorGUILayout.Space(20);
        if (GUILayout.Button("Delete Line")){
            graphLine.DeleteLine();
        }
        else{
            serializedObject.ApplyModifiedProperties();
        }
    }

    private void OnSceneGUI(){
        graphLine.DrawAll();
    }
}
