using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GraphNode))]
[CanEditMultipleObjects]
public class GraphNodeEditor : Editor{
    private GraphNode graphNode;
    private SerializedProperty graphNodeType;
    private SerializedProperty lines;
    private SerializedProperty neighbors;
    void OnEnable(){
        graphNode = (GraphNode) target;
        graphNodeType = serializedObject.FindProperty("graphNodeType");
        lines = serializedObject.FindProperty("lines");
        neighbors = serializedObject.FindProperty("neighbors");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(graphNodeType);
        EditorGUILayout.PropertyField(neighbors);
        EditorGUILayout.PropertyField(lines);
        EditorGUILayout.Space(20);
        
        if (GUILayout.Button("Add Point New Branch")){
            graphNode.AddNodeNewBranch();
        }

        if (IsTwoNotConnectedNodeSelected()){
            if (GUILayout.Button("Connect Nodes")){
                graphNode.ConnectNode(Selection.gameObjects[0].GetComponent<GraphNode>(), Selection.gameObjects[1].GetComponent<GraphNode>());
            }
        }
        
        EditorGUILayout.Space(20);
        if (GUILayout.Button("Delete Point")){
            graphNode.DeletePoint();
        }
        else{
            serializedObject.ApplyModifiedProperties();
        }
    }

    private bool IsTwoNotConnectedNodeSelected(){
        if (Selection.gameObjects.Length != 2) return false;

        return Selection.gameObjects[0].GetComponent<GraphNode>() && Selection.gameObjects[1].GetComponent<GraphNode>();
    }

    private void OnSceneGUI(){
        graphNode.DrawAll();
    }
}
