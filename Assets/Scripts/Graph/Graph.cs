using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Graph : MonoBehaviour{
    [SerializeField] private List<GraphNode> nodes;
    [SerializeField] private List<GraphLine> lines;


    public GraphPath GetCarPath(GraphNode startNode, Car car){
        GraphPath graphPath = new GraphPath();
        graphPath.pathNodes = new List<GraphNode>();
        graphPath.pathNodes.Add(startNode);
        graphPath.pathLines = new List<GraphLine>();

        
        int[] selected = new int[nodes.Count];
        selected[nodes.IndexOf(startNode)] = 1;
        GraphNode currentNode = startNode;
        List<GraphNode> neighbors = currentNode.GetNeighbors();
        int q = 0;
        
        while (neighbors != null && q < 20){
            q++;
            bool has = false;
            for (int i = 0; i < neighbors.Count; i++){
                
                if (selected[nodes.IndexOf(neighbors[i])] != 1){
                    GraphLine currentLine = currentNode.GetNeighborLineByIndex(i);
                    if(currentLine.IsLineFull()) continue;
                    
                    graphPath.pathLines.Add(currentLine);
                    graphPath.pathNodes.Add(neighbors[i]);
                    
                    selected[nodes.IndexOf(neighbors[i])] = 1;
                    currentNode = neighbors[i];
                    neighbors = currentNode.GetNeighbors();
                    has = true;
                    break;
                }
            }

            if (currentNode.IsBlocked()) break;
            if (!has) break;
        }

        graphPath.parkPlace = graphPath.pathLines[graphPath.pathLines.Count-1].GetComponent<RoadGraphLine>().GetEmptyParkPlace(graphPath.pathNodes[graphPath.pathNodes.Count-2]);
        graphPath.pathLines[graphPath.pathLines.Count-1].GetComponent<RoadGraphLine>().OccupyParkPlace(graphPath.parkPlace, car);
        
        //graphPath.Print();
        return graphPath;
    }
    
    //public GraphPath GetPathBetweenTwoNode(GraphNode node1, GraphNode node2);

    #region GraphSetup

    public void PushNode(){
        GraphNode newNode = CreateNode(Vector3.zero);
        if (nodes.Count > 1) CreateLine(newNode, nodes[nodes.Count - 2]);
        Selection.SetActiveObjectWithContext(newNode.gameObject,newNode.gameObject);
    }
    
    public void NewBranchNode(GraphNode node1){
        GraphNode newNode = CreateNode(Vector3.zero);
        CreateLine(newNode, node1);
        Selection.SetActiveObjectWithContext(newNode.gameObject,newNode.gameObject);
    }
    
    public void InsertNode(GraphNode node1, GraphNode node2, GraphLine graphLine){
        GraphNode newNode = CreateNode((node1.transform.position + node2.transform.position) / 2);
        graphLine.node2 = newNode;
        CreateLine(newNode, node2);
        Selection.SetActiveObjectWithContext(newNode.gameObject,newNode.gameObject);
    }

    public void ConnectTwoNode(GraphNode node1, GraphNode node2){
        CreateLine(node1, node2);
    }

    private GraphNode CreateNode(Vector3 _pos){
        GameObject tmpGO = new GameObject("Point");
        tmpGO.transform.SetParent(gameObject.transform);
        tmpGO.transform.position = _pos;
        GraphNode graphNode = tmpGO.AddComponent<GraphNode>();
        graphNode.graph = this;
        nodes.Add(graphNode);
        return graphNode;
    }
    
    private GraphLine CreateLine(GraphNode node1, GraphNode node2){
        GameObject tmpGO = new GameObject("Line");
        tmpGO.transform.SetParent(gameObject.transform);
        GraphLine graphLine = tmpGO.AddComponent<GraphLine>();
        graphLine.graph = this;
        graphLine.node1 = node1;
        graphLine.node2 = node2;
        lines.Add(graphLine);
        return graphLine;
    }

    public void DeletePoint(GraphNode graphNode){
        nodes.Remove(graphNode);
        DestroyImmediate(graphNode.gameObject);

        for (int i = 0; i < lines.Count; i++){
            if (lines[i].node1 == graphNode || lines[i].node2 == graphNode){
                DeleteLine(lines[i]);
                i--;
            }
        }
        
        GameObject toSelect;
        if (nodes.Count > 0) toSelect = nodes[0].gameObject;
        else toSelect = gameObject;
        
        Selection.SetActiveObjectWithContext(toSelect, toSelect);
    }

    public void DeleteLine(GraphLine graphLine){
        lines.Remove(graphLine);
        DestroyImmediate(graphLine.gameObject);
        
        GameObject toSelect;
        if (nodes.Count > 0) toSelect = nodes[0].gameObject;
        else toSelect = gameObject;
        
        Selection.SetActiveObjectWithContext(toSelect, toSelect);
    }

    public void UpdateNeighbors(){
        for (int i = 0; i < nodes.Count; ++i){
            nodes[i].ClearNeighbors();
            nodes[i].gameObject.name = "Node" + i;
        }

        for (int i = 0; i < lines.Count; i++){
            lines[i].node1.AddNeighbor(lines[i].node2, lines[i]);
            lines[i].node2.AddNeighbor(lines[i].node1, lines[i]);
            lines[i].gameObject.name = "Line" + i;
        }
    }
    
    public void ResetGraph(){
        foreach (GraphNode turnPoint in nodes){
            DestroyImmediate(turnPoint.gameObject);
        }

        foreach (GraphLine graphLine in lines){
            DestroyImmediate(graphLine.gameObject);
        }
        
        nodes.Clear();
        lines.Clear();
    }

    #endregion

    #region Draw
    
    public void DrawGraph(){
        DrawLines();
        DrawPoints();
    }
    
    public void DrawPoints(){
        for (int i = 0; i < nodes.Count; i++){
            nodes[i].DrawPoint();
        }
    }
    
    public void DrawLines(){
        for (int i = 0; i < lines.Count; i++){
            lines[i].DrawLine();
        }
    }

    #endregion

}
