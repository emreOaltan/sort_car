using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GraphLine : MonoBehaviour{
    [SerializeField] public Graph graph;
    [SerializeField] public GraphNode node1;
    [SerializeField] public GraphNode node2;


    public bool IsLineFull() => !GetComponent<RoadGraphLine>().HasEmptyParkPlace();

    public void DeleteLine(){
        graph.DeleteLine(this);
    }

    public void InsertNode(){
        graph.InsertNode(node1, node2, this);
    }
    
    
    public void DrawAll(){
        graph.DrawGraph();
    }
    
    public void DrawLine(){
        Handles.color = Color.red;
        Handles.DrawLine(node1.transform.position, node2.transform.position, 6f);
        transform.position = (node1.transform.position + node2.transform.position) / 2;
        
        Handles.color = Color.yellow;
        if (Handles.Button(transform.position, Quaternion.identity, 3f, 3f, Handles.SphereHandleCap)){
            Selection.SetActiveObjectWithContext(gameObject, gameObject);
        }
    }
}
