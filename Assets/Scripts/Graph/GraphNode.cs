using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum GraphNodeType{StartNode,Intermediate, EndNode}

[ExecuteAlways]
public class GraphNode : MonoBehaviour{
    [SerializeField] private GraphNodeType graphNodeType;
    [SerializeField] public Graph graph;
    [SerializeField] private List<GraphNode> neighbors;
    [SerializeField] private List<GraphLine> lines;

    
    private void OnValidate(){
        UpdateSortedNeighbors();
    }


    public List<GraphNode> GetNeighbors() => neighbors;
    public bool IsBlocked() => GetComponent<RoadGraphNode>().IsBlocked;
    
    public GraphLine GetNeighborLineByIndex(int index) => lines[index];

    public void CheckNodeBlocked(){
        for (int i = 0; i < lines.Count; i++){
            if (lines[i].IsLineFull() && GetComponent<RoadGraphNode>().IsFullLineBlocks[i]){
                GetComponent<RoadGraphNode>().IsBlocked = true;
            }
        }
    }

    public void AddNeighbor(GraphNode node, GraphLine line){
        neighbors.Add(node);
        lines.Add(line);
    }
    
    public void RemoveNeighbor(GraphNode node){
        neighbors.Remove(node);
    }

    public void UpdateSortedNeighbors(){
        neighbors.Clear();
        for (int i = 0; i < lines.Count; i++){
            neighbors.Add(lines[i].node1 == this ? lines[i].node2 : lines[i].node1);
        }
    }

    public void ClearNeighbors(){
        neighbors?.Clear();
        lines?.Clear();
    }

    public void AddNodeNewBranch(){
        graph.NewBranchNode(this);
    }

    public void ConnectNode(GraphNode node1, GraphNode node2){
        graph.ConnectTwoNode(node1, node2);
    }

    public void DeletePoint(){
        graph.DeletePoint(this);
    }
    
    public void DrawAll(){
        graph.DrawGraph();
    }
    
    public void DrawPoint(){
        Handles.color = Color.green;
        if (Handles.Button(transform.position, Quaternion.identity, 5f, 5f, Handles.CubeHandleCap)){
            Selection.SetActiveObjectWithContext(gameObject, gameObject);
        }
    }
}
