using System.Collections.Generic;
using UnityEngine;

public struct GraphPath{
    public List<GraphLine> pathLines;
    public List<GraphNode> pathNodes;
    public ParkPlace parkPlace;
    
}
