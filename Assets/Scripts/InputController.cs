using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour{
    [SerializeField] private Camera m_Camera;
    private Ray ray;
    private RaycastHit raycastHit;
    
    void Update()
    {
        if (GameController.gameState == GameState.Playing && Input.GetMouseButtonDown(0)){
            SendRay();
        }
    }

    private void SendRay(){
        ray = m_Camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out raycastHit, 500f)){
            MyButton myButton = raycastHit.transform.GetComponent<MyButton>();
            if (myButton != null){
                PressButton(myButton);
            }
        }
    }

    private void PressButton(MyButton button){
        button.Press();
    }
}
