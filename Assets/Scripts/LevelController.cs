using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[ExecuteAlways]
public class LevelController : MonoBehaviour{
    public static LevelController Instance;

    LevelController(){
        Instance = this;
        materialUpdate = new UnityEvent();
    }

    [Range(0.2f,10f)] public float carSpeed = 1;
    public float doorOpeningTime = 0.5f, doorOpenedWaitTime = 0.5f;
    
    [SerializeField] private int purpleParkPlaceCount;
    public int PurpleParkPlaceCount => purpleParkPlaceCount;
    
    [SerializeField] private int yellowParkPlaceCount;
    public int YellowParkPlaceCount => yellowParkPlaceCount;

    [SerializeField] public Color purpleColor, yellowColor;
    
    [Space(50)]
    [SerializeField] private Material purpleMaterial;
    [SerializeField] private Material yellowMaterial;
    [NonSerialized] public UnityEvent materialUpdate;


    private void OnValidate(){
        materialUpdate.Invoke();
        UpdateMaterials();
        
    }

    private void UpdateMaterials(){
        purpleMaterial.SetColor("_Color", purpleColor);
        yellowMaterial.SetColor("_Color", yellowColor);
    }
}
