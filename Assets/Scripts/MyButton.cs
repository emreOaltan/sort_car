using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MyClickEvent : UnityEvent<System.Object> { }

public class MyButton : MonoBehaviour{
    public GameObject buttonPressObject;
    public MyClickEvent buttonClickEvent;
    public bool isPressed = false;


    public void Press(){
        if(isPressed) return;
        isPressed = true;
        buttonClickEvent.Invoke(this);
        buttonPressObject.transform.DOLocalMoveY(-2.4f, 0.3f).SetLoops(2, LoopType.Yoyo).Play();
    }

    public void Release(){
        if(!isPressed) return;
        isPressed = false;
        
    }

}
