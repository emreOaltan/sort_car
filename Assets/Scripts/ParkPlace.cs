using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorType{PURPLE, YELLOW}

[ExecuteAlways]
public class ParkPlace : MonoBehaviour{
    public ColorType colorType;
    
    
    [Space(50)]
    public SpriteRenderer spriteRenderer;
    public Color purple, yellow;

    [SerializeField] private Car parkedCar;

    public void SetParkedCar(Car _car) => parkedCar = _car;
    public bool IsOccupied() => parkedCar != null;

    private void OnValidate(){
        UpdateColor();
    }

    private void Awake(){
        LevelController.Instance.materialUpdate.AddListener(UpdateColor);
    }

    private void UpdateColor(){
        if (spriteRenderer != null){
            spriteRenderer.color = colorType == ColorType.PURPLE ? LevelController.Instance.purpleColor : LevelController.Instance.yellowColor;
        }
    }
    
}
