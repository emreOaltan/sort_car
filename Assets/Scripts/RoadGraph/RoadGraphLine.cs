using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGraphLine : MonoBehaviour{
    [SerializeField] private List<ParkPlace> parkPlaces;
    [SerializeField] private int remainedParkPlaceCount;

    private void Start(){
        remainedParkPlaceCount = parkPlaces.Count;
    }

    public void OccupyParkPlace(ParkPlace parkPlace, Car car){
        if(parkPlace == null) return;
        parkPlace.SetParkedCar(car);
        remainedParkPlaceCount--;
        UpdateBlockness();
    }

    private void UpdateBlockness(){
        GetComponent<GraphLine>().node1.CheckNodeBlocked();
        GetComponent<GraphLine>().node2.CheckNodeBlocked();
    }

    public bool HasEmptyParkPlace() => remainedParkPlaceCount > 0 || parkPlaces.Count == 0;

    public ParkPlace GetEmptyParkPlace(GraphNode farNode){
        float max = 0;
        int maxIndex = -1;
        for (int i = parkPlaces.Count - 1; i >= 0; i--){
            if (!parkPlaces[i].IsOccupied()){
                float dist = (farNode.transform.position - parkPlaces[i].transform.position).sqrMagnitude;
                if (dist > max){
                    max = dist;
                    maxIndex = i;
                }
            }
        }

        if (maxIndex == -1) return null;
        else return parkPlaces[maxIndex];
    }
}
