using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RoadGraphNode : MonoBehaviour{

    [SerializeField] private List<bool> isFullLineBlocks;
    [SerializeField] private bool _isBlocked = false;


    public List<bool> IsFullLineBlocks{
        get => isFullLineBlocks;
        set => isFullLineBlocks = value;
    }

    public bool IsBlocked{
        get => _isBlocked;
        set => _isBlocked = value;
    }
}
