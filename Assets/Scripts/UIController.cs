using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour{

    [SerializeField] private GameObject successCanvas, failCanvas;


    public void RestartGame() => GameController.Instance.RestartGame();
    public void NextLevel() => GameController.Instance.NextLevel();
    
    public void LevelFinished(bool isSuccess){
        if (isSuccess){
            successCanvas.SetActive(true);
        }
        else{
            failCanvas.SetActive(true);
        }
    }
}
